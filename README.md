# Java Starter project

Copy and paste this to begin a new project. This incorporates:

 - Spring Boot project framework.
   - Starter heartbeat endpoint.
 - A useful `.gitignore` file.
 - Dockerfile that packages and runs the app.
 - `.gitlab_ci` file
   - Test runs on develop branches, with report artifacts saved.
   - Builds Docker Image and places it in GitLab repository with version tag.

## To get started:

 - Create a new repo. Copy all files (except the `.git` repo!)
 - Edit `pom.xml` to replace 'starter' with the name of the codebase.
 - Check into the `master` branch, then create `develop` branch.

## To run:

```
docker build .
docker run <image id>
```

Visit http://localhost:8080/heartbeat