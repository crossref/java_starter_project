package org.crossref.starter.heartbeat;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HeartbeatController {
    @GetMapping(value = "/heartbeat")
    public Heartbeat greeting() {
        return new Heartbeat();
    }
}
