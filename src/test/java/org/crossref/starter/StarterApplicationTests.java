package org.crossref.starter;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import java.io.IOException;

@SpringBootTest
class StarterApplicationTests {

	@Test
	void simple() throws IOException {
		Assertions.assertEquals(10, 10, "Ten should equal ten.");
	}
}
